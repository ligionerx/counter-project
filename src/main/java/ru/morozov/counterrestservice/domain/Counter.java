package ru.morozov.counterrestservice.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * Counter entity.
 *
 * @author andrey-morozov
 */
@Entity
@Setter
@Getter
@EqualsAndHashCode
@ToString
public class Counter {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * Counter name. Unique value.
     */
    @Column(unique = true, name = "name")
    private String name;

    /**
     * Counter value.
     */
    @Column(name = "count_value")
    private long count;
}
