package ru.morozov.counterrestservice.service.impl;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.morozov.counterrestservice.api.dto.CounterDto;
import ru.morozov.counterrestservice.domain.Counter;
import ru.morozov.counterrestservice.exceptions.CounterNotFoundException;
import ru.morozov.counterrestservice.mapper.CounterMapper;
import ru.morozov.counterrestservice.repository.CounterRepository;
import ru.morozov.counterrestservice.service.CounterService;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * CounterServiceImpl
 */
@Service
@NoArgsConstructor
@Slf4j
public class CounterCounterServiceImpl implements CounterService {
    private static final String LOG_TAG = "[COUNTER_SERVICE_IMPL] :: ";
    @Lazy
    @Autowired
    private CounterMapper mapper;
    @Lazy
    @Autowired
    private CounterRepository repository;

    @Override
    @Transactional
    public CounterDto createCounter(String name) {
        log.info("{}createCounter() - start:: save Counter with name: {}", LOG_TAG, name);
        Counter counter = new Counter();
        counter.setName(name);
        counter.setCount(0L);
        Counter savedCounter = repository.save(counter);
        CounterDto dto = mapper.mapToDto(savedCounter);
        log.info("{}createCounter() - finish:: {}", LOG_TAG, dto);
        return dto;
    }

    @Override
    @Transactional
    public CounterDto incrementCountValueByName(String name) throws CounterNotFoundException {
        log.info("{}incrementCountValueByName() - start:: increment Counter with name: {}", LOG_TAG, name);
        Counter counter = repository.findByName(name).orElseThrow(CounterNotFoundException::new);
        long countValue = counter.getCount();
        countValue += 1;
        counter.setCount(countValue);
        Counter savedCounter = repository.save(counter);
        CounterDto dto = mapper.mapToDto(savedCounter);
        log.info("{}incrementCountValueByName() - finish:: {}", LOG_TAG, dto);
        return dto;
    }

    @Override
    public Long getCountValueByName(String name) throws CounterNotFoundException {
        log.info("{}getCountValueByName() - start:: get count for Counter with name: {}", LOG_TAG, name);
        Counter counter = repository.findByName(name).orElseThrow(CounterNotFoundException::new);
        long count = counter.getCount();
        log.info("{}getCountValueByName() - finish:: count value = {}", LOG_TAG, count);
        return count;
    }

    @Override
    @Transactional
    public void deleteCounterByName(String name) throws CounterNotFoundException {
        log.info("{}deleteCounterByName() - start:: delete Counter with name: {}", LOG_TAG, name);
        repository.findByName(name).orElseThrow(CounterNotFoundException::new);
        repository.deleteByName(name);
        log.info("{}deleteCounterByName() - finish:: delete Counter with name: {} completed", LOG_TAG, name);
    }

    @Override
    public Long getCountTotalValue() {
        log.info("{}getCountTotalValue() - start:: ", LOG_TAG);
        List<Counter> allCounterList = repository.findAll();
        Optional<Long> resultTotalCount = allCounterList.stream()
                .filter(Objects::nonNull)
                .map(Counter::getCount)
                .reduce(Long::sum);
        Long totalValue = 0L;
        if (resultTotalCount.isPresent()) {
            totalValue = resultTotalCount.get();
        }
        log.info("{}getCountTotalValue() - finish:: total count = {}", LOG_TAG, totalValue);
        return totalValue;
    }

    @Override
    public List<String> getAllCountNameList() {
        log.info("{}getAllCountNameList() - start:: ", LOG_TAG);
        List<Counter> allCounterList = repository.findAll();
        List<String> countersNameList = allCounterList.stream()
                .filter(Objects::nonNull)
                .map(Counter::getName)
                .collect(Collectors.toList());
        log.info("{}getAllCountNameList() - finish:: {}", countersNameList, LOG_TAG);
        return countersNameList;
    }
}
