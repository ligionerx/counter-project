package ru.morozov.counterrestservice.service;

import ru.morozov.counterrestservice.api.dto.CounterDto;
import ru.morozov.counterrestservice.domain.Counter;
import ru.morozov.counterrestservice.exceptions.CounterNotFoundException;

import java.util.List;

/**
 * ServiceInterface for CounterServiceImpl
 */
public interface CounterService {
    /**
     * Create new {@link Counter}
     * @param name  {@link Counter} name
     * @return      {@link CounterDto}
     */
    CounterDto createCounter(String name);

    /**
     * Increment count value by {@link Counter} name
     * @param name  {@link Counter} name
     * @return      {@link CounterDto}
     */
    CounterDto incrementCountValueByName(String name) throws CounterNotFoundException;

    /**
     * Get count value by {@link Counter} name
     * @param name  {@link Counter} name
     * @return      {@link Long} count value
     */
    Long getCountValueByName (String name) throws CounterNotFoundException;

    /**
     * Delete {@link Counter} by name
     * @param name  {@link Counter} name
     */
    void deleteCounterByName(String name) throws CounterNotFoundException;

    /**
     * Get total count value
     * @return      {@link Long} total count value
     */
    Long getCountTotalValue();

    /**
     * Get all {@link Counter} names list
     * @return {@link List<String>} names list
     */
    List<String> getAllCountNameList();
}
