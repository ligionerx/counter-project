package ru.morozov.counterrestservice.exceptions;

/**
 * CounterNotFoundException.
 *
 * @author andrey-morozov
 */
public class CounterNotFoundException extends Exception{
    public CounterNotFoundException() {
        super("Counter not found");
    }
}
