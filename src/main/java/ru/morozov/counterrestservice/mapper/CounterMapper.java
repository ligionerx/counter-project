package ru.morozov.counterrestservice.mapper;

import org.springframework.stereotype.Component;
import ru.morozov.counterrestservice.api.dto.CounterDto;
import ru.morozov.counterrestservice.domain.Counter;

import java.util.Objects;
@Component
public class CounterMapper {
    public CounterDto mapToDto(Counter counter) {
        if (Objects.isNull(counter)) {
            return null;
        }
        CounterDto dto = new CounterDto();
        dto.setName(counter.getName());
        dto.setCount(Long.toString(counter.getCount()));
        return dto;
    }
}
