package ru.morozov.counterrestservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CounterRestServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CounterRestServiceApplication.class, args);
	}

}
