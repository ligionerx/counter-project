package ru.morozov.counterrestservice.api.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.morozov.counterrestservice.api.dto.CounterDto;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.List;

/**
 * RestControllerInterface.
 */
@RequestMapping("/counter")
public interface CounterRestController {
    @PostMapping(
            value = "/create",
            produces = APPLICATION_JSON_VALUE
    )
    ResponseEntity<CounterDto> handleCreateCounterByName(String name);

    @PutMapping(
            value = "/increment",
            produces = APPLICATION_JSON_VALUE
    )
    ResponseEntity<CounterDto> handleIncrementCountValueByName(String name);

    @GetMapping(
            value = "/countValue",
            produces = APPLICATION_JSON_VALUE
    )
    ResponseEntity<Long> handleGetCountValueByName(String name);

    @DeleteMapping(
            value = "/deleteCount"
    )
    ResponseEntity<Void> handleDeleteCounterByName(String name);

    @GetMapping(
            value = "/countsTotalValue",
            produces = APPLICATION_JSON_VALUE
    )
    ResponseEntity<Long> handleGetCountTotalValue();

    @GetMapping(
            value = "/allCountName",
            produces = APPLICATION_JSON_VALUE
    )
    ResponseEntity<List<String>> handleGetAllCountNameList();
}
