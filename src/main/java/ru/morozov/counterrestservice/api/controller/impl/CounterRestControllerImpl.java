package ru.morozov.counterrestservice.api.controller.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.morozov.counterrestservice.api.controller.CounterRestController;
import ru.morozov.counterrestservice.api.dto.CounterDto;
import ru.morozov.counterrestservice.exceptions.CounterNotFoundException;
import ru.morozov.counterrestservice.service.impl.CounterCounterServiceImpl;

import java.util.List;

@Slf4j
@RestController
public class CounterRestControllerImpl implements CounterRestController {
    private static final String LOG_TAG = "[REST_CONTROLLER_IMPL] :: ";
    @Lazy
    @Autowired
    private CounterCounterServiceImpl counterService;

    @Override
    public ResponseEntity<CounterDto> handleCreateCounterByName(@RequestParam String name) {
        log.info("{}handleCreateCounterByName() - start:: {}", LOG_TAG, name);
        CounterDto counterDto = counterService.createCounter(name);
        log.info("{}handleCreateCounterByName() - finish:: {}", LOG_TAG, counterDto);
        return ResponseEntity.ok(counterDto);
    }

    @Override
    public ResponseEntity<CounterDto> handleIncrementCountValueByName(@RequestParam String name) {
        log.info("{}handleIncrementCountValueByName() - start:: {}", LOG_TAG, name);
        CounterDto counterDto = null;
        try {
            counterDto = counterService.incrementCountValueByName(name);
        } catch (CounterNotFoundException e) {
            log.error("{}Counter with name {} not found", LOG_TAG, name, e);
            return ResponseEntity.notFound().build();
        }
        log.info("{}handleIncrementCountValueByName() - finish:: {}", LOG_TAG, counterDto);
        return ResponseEntity.ok(counterDto);
    }

    @Override
    public ResponseEntity<Long> handleGetCountValueByName(@RequestParam String name) {
        log.info("{}handleGetCountValueByName() - start:: {}", LOG_TAG, name);
        Long countValueByName = null;
        try {
            countValueByName = counterService.getCountValueByName(name);
        } catch (CounterNotFoundException e) {
            log.error("{}Counter with name {} not found", LOG_TAG, name, e);
            return ResponseEntity.notFound().build();
        }
        log.info("{}handleGetCountValueByName() - finish:: count value = {}", LOG_TAG, countValueByName);
        return ResponseEntity.ok(countValueByName);
    }

    @Override
    public ResponseEntity<Void> handleDeleteCounterByName(@RequestParam String name) {
        log.info("{}handleDeleteCounterByName() - start:: {}", LOG_TAG, name);
        try {
            counterService.deleteCounterByName(name);
        } catch (CounterNotFoundException e) {
            log.error("{}Counter with name {} not found", LOG_TAG, name, e);
            return ResponseEntity.notFound().build();
        }
        log.info("{}handleDeleteCounterByName() - finish:: counter with name {} successfully deleted", LOG_TAG, name);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Long> handleGetCountTotalValue() {
        log.info("{}handleGetCountTotalValue() - start::", LOG_TAG);
        Long countTotalValue = counterService.getCountTotalValue();
        log.info("{}handleGetCountTotalValue() - finish:: count total value = {}", LOG_TAG, countTotalValue);
        return ResponseEntity.ok(countTotalValue);
    }

    @Override
    public ResponseEntity<List<String>> handleGetAllCountNameList() {
        log.info("{}handleGetAllCountNameList() - start::", LOG_TAG);
        List<String> allCountNameList = counterService.getAllCountNameList();
        log.info("{}handleGetAllCountNameList() - finish:: {}", LOG_TAG, allCountNameList);
        return ResponseEntity.ok(allCountNameList);
    }
}
