package ru.morozov.counterrestservice.api.dto;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class CounterDto implements Serializable {
    private String name;
    private String count;
}
