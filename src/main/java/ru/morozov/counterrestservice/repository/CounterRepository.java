package ru.morozov.counterrestservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.morozov.counterrestservice.domain.Counter;

import java.util.Optional;

/**
 * Repository.
 *
 * @author andrey-morozov
 */

@Repository
public interface CounterRepository extends JpaRepository<Counter, Long> {

    Optional<Counter> findByName(String name);

    void deleteByName(String name);
}
