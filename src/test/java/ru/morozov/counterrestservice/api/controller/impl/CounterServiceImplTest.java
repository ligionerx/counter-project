package ru.morozov.counterrestservice.api.controller.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.morozov.counterrestservice.api.dto.CounterDto;
import ru.morozov.counterrestservice.domain.Counter;
import ru.morozov.counterrestservice.exceptions.CounterNotFoundException;
import ru.morozov.counterrestservice.mapper.CounterMapper;
import ru.morozov.counterrestservice.repository.CounterRepository;
import ru.morozov.counterrestservice.service.impl.CounterCounterServiceImpl;

import java.util.Optional;

/**
 * CounterServiceImplTest.
 *
 * @author andrey-morozov
 */
@ExtendWith(MockitoExtension.class)
public class CounterServiceImplTest {
    @Mock
    CounterMapper counterMapper;
    @Mock
    CounterRepository counterRepository;

    @Captor
    ArgumentCaptor<Counter> counterArgumentCaptor;

    @InjectMocks
    CounterCounterServiceImpl subj;

    @Test
    void createCounterTest() {
        String name = "TestName";
        Counter counter = newCounter(name, 0L);
        when(counterRepository.save(counterArgumentCaptor.capture())).thenReturn(counter);
        CounterDto actual = newCounterDto(counter);
        when(counterMapper.mapToDto(counter)).thenReturn(actual);

        CounterDto expected = subj.createCounter(name);
        Counter expectedCounter = counterArgumentCaptor.getValue();
        assertThat(expected, is(actual));
        assertThat(expectedCounter, is(counter));
        verify(counterRepository, times(1)).save(counter);
        verify(counterMapper, times(1)).mapToDto(counter);
    }

    @Test
    void incrementCountValueByNameTest() throws CounterNotFoundException {
        String name = "TestName";
        long actualCount = 10L;
        Counter counter = newCounter(name, actualCount);
        when(counterRepository.findByName(name)).thenReturn(Optional.of(counter));
        Counter counterWithIncrementCount = incrementCountValue(counter);
        when(counterRepository.save(counterArgumentCaptor.capture())).thenReturn(counterWithIncrementCount);
        CounterDto actual = newCounterDto(counterWithIncrementCount);
        when(counterMapper.mapToDto(counterWithIncrementCount)).thenReturn(actual);

        CounterDto expected = subj.incrementCountValueByName(name);
        Counter expectedCounter = counterArgumentCaptor.getValue();
        assertThat(expected, is(actual));
        assertThat(expectedCounter.getCount(), is(counterWithIncrementCount.getCount()));
        assertThat(expectedCounter.getName(), is(name));
        verify(counterRepository, times(1)).save(counter);
        verify(counterMapper, times(1)).mapToDto(counterWithIncrementCount);
    }

    @Test
    void incrementCountValueByNameExceptionTest() {
        String name = "TestName";
        when(counterRepository.findByName(name)).thenReturn(Optional.empty());
        Assertions.assertThrows(CounterNotFoundException.class, () -> subj.incrementCountValueByName(name));
    }

    Counter incrementCountValue(Counter counter) {
        long count = counter.getCount();
        count += 1;
        counter.setCount(count);
        return counter;
    }

    CounterDto newCounterDto(Counter counter){
        CounterDto result = newCounterDto();
        result.setCount(Long.toString(counter.getCount()));
        result.setName(counter.getName());
        return result;
    }
    CounterDto newCounterDto(){
        return new CounterDto();
    }
    Counter newCounter(String name, long count) {
        Counter result = newCounter();
        result.setName(name);
        result.setCount(count);
        return result;
    }

    Counter newCounter() {
        return new Counter();
    }
}
